const app = angular.module("Candidate.App", []);

app.component("itmRoot", {
    controller: class {
        constructor() {
            this.candidates = [];
            this.totalVotes = 0;
        }

        $onInit() {
            this.candidates = [
                { name: "Puppies", votes: 10 }, 
                { name: "Kittens", votes: 12 }, 
                { name: "Gerbils", votes: 7 }
            ];
            var total = 0;
            angular.forEach(this.candidates, function(candidate) { 
                total += candidate.votes; 
            });
            this.totalVotes = total;
        }

        onVote(candidate) {
            candidate.votes += 1;
            this.totalVotes += 1;
            console.log(`Vote for ${candidate.name}`);
        }

        onAddCandidate(candidate) {
            this.candidates.push({name: candidate.name, votes: 0})
            console.log(`Added candidate ${candidate.name}`);
        }

        onRemoveCandidate(candidate) {
            this.totalVotes -= candidate.votes;
            var index = this.candidates.indexOf(candidate);
            this.candidates.splice(index, 1);
            console.log(`Removed candidate ${candidate.name}`);
        }
    },
    template: `
        <div class="jumbotron text-center">
            <h1>Which candidate brings the most joy?</h1>
        </div>

        <div class="container">
            <itm-results 
                candidates="$ctrl.candidates"
                total-votes="$ctrl.totalVotes">
            </itm-results>

            <itm-vote 
                candidates="$ctrl.candidates"
                on-vote="$ctrl.onVote($candidate)">
            </itm-vote>

            <itm-management 
                candidates="$ctrl.candidates"
                on-add="$ctrl.onAddCandidate($candidate)"
                on-remove="$ctrl.onRemoveCandidate($candidate)">
            </itm-management>
        </div>
    `
});

app.component("itmManagement", {
    bindings: {
        candidates: "<",
        onAdd: "&",
        onRemove: "&"
    },
    controller: class {
        constructor() {
            this.newCandidate = {
                name: ""
            };
            this.duplicateCandidate = false;
        }

        submitCandidate(candidate) {
            if (this.candidates.map(a=>a.name.toLowerCase()).includes(candidate.name.toLowerCase())) {
                this.duplicateCandidate = true;
                console.log(`Can't repeat a name!`);
            } else {
                this.duplicateCandidate = false;
                this.onAdd({ $candidate: candidate });
            }
            this.newCandidate.name = ""
        }

        removeCandidate(candidate) {
            this.onRemove({ $candidate: candidate });
        }
    },
    template: `
        <h2>Manage Candidates</h2>

        <h4>Add New Candidate</h4>
        <form name="addForm" ng-submit="$ctrl.submitCandidate($ctrl.newCandidate)" novalidate>

            <label>Candidate Name</label>
            <input name="newName" type="text" ng-model="$ctrl.newCandidate.name" required>
            <button type="submit" ng-disabled="addForm.$invalid" class="btn btn-success">Add</button>
            <p ng-show="$ctrl.duplicateCandidate" class="text-danger">That candidate already exists</p>
        </form>

        <h4>Remove Candidate</h4>
        <ul>
            <li ng-repeat="candidate in $ctrl.candidates | orderBy:'name' ">
                <span ng-bind="candidate.name"></span>
                <button type="button" ng-click="$ctrl.removeCandidate(candidate)" class="btn btn-small text-danger">X</button>
            </li>
        </ul>
    `
});

app.component("itmVote", {
    bindings: {
        candidates: "<",
        onVote: "&"
    },
    controller: class {},
    template: `
        <h2>Cast your vote!</h2>

        <button type="button" 
            ng-repeat="candidate in $ctrl.candidates | orderBy:'name' "
            ng-click="$ctrl.onVote({ $candidate: candidate })"
            class="btn btn-primary mr-2 mb-4">
            <span ng-bind="candidate.name"></span>
        </button>
    `
});

app.component("itmResults", {
    bindings: {
        candidates: "<",
        totalVotes: "<"
    },
    controller: class {},
    template: `
        <h2>Live Results</h2>
        <ul>
            <li ng-repeat="candidate in $ctrl.candidates | orderBy:'-votes' ">
                <span ng-bind="candidate.name"></span> have
                <strong ng-bind="candidate.votes"></strong> votes with 
                <strong ng-bind="100 * candidate.votes / $ctrl.totalVotes"></strong>% of the vote
            </li>
        </ul>
    `
});
